<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

    	$userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();

        return $this->render('default/index.html.twig',
        	array(
        		'users' =>   $users,
        	)/*, 
        	['base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]*/);
    }

}
